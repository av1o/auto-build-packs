#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

set -e

# prepare

echo -e "---> ${GREEN}Preparing${NC}"

# create the dockerconfigjson
echo -e "Injecting authentication for ${RED}$CI_REGISTRY_USER${NC}@${GREEN}$CI_REGISTRY${NC}"
echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /home/cnb/.docker/config.json

CI_PROJECT_DIR="${CI_PROJECT_DIR:-"/workspace/source"}"
PROJECT_PATH="${PROJECT_PATH:-$SOURCE_SUBPATH}"
USER_ID=$(id -u)
GROUP_ID=$(id -g) # use gid 0 for openshift compat
RUN_IMAGE="${AUTO_CNB_RUN_IMAGE:-"docker.io/paketobuildpacks/run:base-cnb"}"
if [[ -n "${PROJECT_PATH// }" ]]; then
	CI_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$PROJECT_PATH"
fi

CNB_PLATFORM_DIR="${CNB_PLATFORM_DIR:-/platform}"

echo "BuildPack configuration:"
printf "%s" "$CI_JOB_STARTED_AT" > "$CNB_PLATFORM_DIR/env/BP_OCI_CREATED"
printf "%s" "$CI_COMMIT_SHA" > "$CNB_PLATFORM_DIR/env/BP_OCI_REVISION"
printf "%s" "$CI_COMMIT_REF_NAME" > "$CNB_PLATFORM_DIR/env/BP_OCI_REF_NAME"
printf "%s" "$CI_PROJECT_TITLE" > "$CNB_PLATFORM_DIR/env/BF_OCI_TITLE"


for var in "${!BP_@}"; do
	printf "\t%s" "$var"
	printf "%s" "${!var}" > "$CNB_PLATFORM_DIR/env/$var"
done
echo

echo -e "Running as ${USER_ID}:${GROUP_ID} with run-image ${GREEN}${RUN_IMAGE}${NC}"

if [ "${GROUP_ID}" -ne 0 ]; then
	echo -e "OpenShift compatible: ${RED}✘${NC} (set GROUP_ID to 0)"
else
	echo -e "OpenShift compatible: ${GREEN}✔${NC}"
fi

#chown -R "${USER_ID}:${GROUP_ID}" "/layers" && \
#	chown -R "${USER_ID}:${GROUP_ID}" "/cache" && \
#	chown -R "${USER_ID}:${GROUP_ID}" "${CI_PROJECT_DIR}"

# detect

echo -e "---> ${GREEN}Detecting${NC}"

/cnb/lifecycle/detector \
	-app="$CI_PROJECT_DIR/${PROJECT_PATH}" \
	-group=/layers/group.toml \
	-plan=/layers/plan.toml

# analyse

echo -e "---> ${GREEN}Analyzing${NC}"

/cnb/lifecycle/analyzer \
	-layers=/layers \
	-group=/layers/group.toml \
	-analyzed=/layers/analyzed.toml \
	-cache-dir=/cache \
	-log-level=debug \
	"${CI_REGISTRY_IMAGE}:${CI_COMMIT_BEFORE_SHA}"

# restore

echo -e "---> ${GREEN}Restoring${NC}"

/cnb/lifecycle/restorer \
	-group=/layers/group.toml \
	-layers=/layers \
	-log-level=debug \
	-cache-dir=/cache

# build

echo -e "---> ${GREEN}Building${NC}"

/cnb/lifecycle/builder \
	-app="$CI_PROJECT_DIR/${PROJECT_PATH}" \
	-layers=/layers \
	-group=/layers/group.toml \
	-plan=/layers/plan.toml

# export

echo -e "---> ${GREEN}Exporting${NC}"

IMAGES="${CI_APPLICATION_IMAGE} $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
if [ -n "${CI_COMMIT_BRANCH}" ]; then
	# countable tag that can be used by flux2
	# https://fluxcd.io/docs/components/image/imagepolicies/#examples
	IMAGES="${IMAGES} $CI_REGISTRY_IMAGE:${CI_COMMIT_BRANCH/\//-}-${CI_COMMIT_SHORT_SHA:0:7}-$(date +%s)"
	IMAGES="${IMAGES} $CI_REGISTRY_IMAGE:${CI_COMMIT_BRANCH/\//-}"
fi
if [ -n "${CI_COMMIT_TAG}" ]; then
	IMAGES="${IMAGES} $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG}"
fi
if [ -n "${CNB_TAGS:-}" ]; then
	IMAGES="${IMAGES} $(echo "${CNB_TAGS}" | tr ',' '\n' | while read -r tag; do echo " $CI_REGISTRY_IMAGE:$tag"; done)"
fi

echo -e "Exporting tags: ${GREEN}[${IMAGES}]${NC}"

/cnb/lifecycle/exporter \
	-app="${CI_PROJECT_DIR}/${PROJECT_PATH}" \
	-layers=/layers \
	-analyzed=/layers/analyzed.toml \
	-group=/layers/group.toml \
	-cache-dir=/cache \
	-run-image="${RUN_IMAGE}" \
	${IMAGES}

echo -e "---> ${GREEN}Signing${NC}"

if [[ -z "$CNB_SIGN_ENABLED" || -z "$COSIGN_PRIVATE_KEY" || -z "$COSIGN_PASSWORD" ]]; then
	echo "Skipping (make sure CNB_SIGN_ENABLED, COSIGN_PRIVATE_KEY, COSIGN_PASSWORD are set)"
	exit 0
fi

echo "Using cosign key: $COSIGN_PRIVATE_KEY"

for image in ${IMAGES}; do
	echo "Signing image: '$image'"
	cosign sign \
		--key "$COSIGN_PRIVATE_KEY" \
		-a "ci.commit.sha=$CI_COMMIT_SHA" \
		-a "ci.commit.ref_slug=$CI_COMMIT_REF_SLUG" \
		-a "ci.job.finished_at=$(date)" \
		-a "ci.job.url=$CI_JOB_URL" \
		-a "ci.job.image=$CI_JOB_IMAGE" \
		-a "ci.project.url=$CI_PROJECT_URL" \
		-a "ci.project.classification=${CI_PROJECT_CLASSIFICATION_LABEL:-"none"}" \
		-a "ci.pipeline.url=$CI_PIPELINE_URL" \
		-a "ci.runner.version=$CI_RUNNER_VERSION" \
		"$image"
done
