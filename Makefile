.PHONY: google
google:
	docker pull gcr.io/buildpacks/builder:v1
	docker build -t abp/google -f google.Dockerfile .
.PHONY: paketo-base
paketo-base:
	docker pull gcr.io/paketo-buildpacks/builder:base
	docker build -t abp/paketo-base -f paketo-base.Dockerfile .
