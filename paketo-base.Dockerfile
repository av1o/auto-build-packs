FROM gcr.io/projectsigstore/cosign:v1.5.2 as COSIGN
ARG PREFIX="gitlab.dcas.dev/open-source/dependency_proxy/containers"
FROM $PREFIX/paketobuildpacks/builder:base AS BUILDER

# create directories as root
USER 0

# modify the cnb user to give it
# root group access
RUN usermod -g root cnb

RUN mkdir -p /cache /layers /workspace /home/cnb/.docker /workspace/source && \
	chown -R 1000:0 /cache /layers /workspace /platform /home/cnb/.docker /workspace/source

#FROM scratch
## flatten the image to avoid issues
## with early versions of Containerd and AWS Fargate
#COPY --from=BUILDER / /
COPY --from=COSIGN /ko-app/cosign /usr/local/bin/

USER cnb

ENV CNB_USER_ID=1000
ENV CNB_GROUP_ID=0

COPY --chown=1000:0 ./run.sh /run.sh
RUN chmod +x /run.sh

# required so that we can trick the procfile
ARG stack_id="io.paketo.stacks.tiny"
LABEL io.buildpacks.stack.id=${stack_id}
ENV CNB_STACK_ID=${stack_id}

ENTRYPOINT ["/run.sh"]
