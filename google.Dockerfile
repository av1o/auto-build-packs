FROM gcr.io/buildpacks/builder:v1

# create directories as root
USER 0

RUN usermod -g root cnb

RUN mkdir -p /cache /layers /workspace /home/cnb/.docker /workspace/source && \
	chown -R 1000:0 /cache /layers /workspace /platform /home/cnb/.docker /workspace/source

USER cnb

ENV CNB_USER_ID=1000
ENV CNB_GROUP_ID=0

COPY --chown=1000:0 ./run.sh /run.sh
RUN chmod +x /run.sh

ENTRYPOINT ["/run.sh"]