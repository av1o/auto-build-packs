# Auto Build Packs

This project provides an implementation of the CNB Lifecycle for use in CI.

> This project is designed with GitLab CI in mind, however it is generic enough that
> it could be used elsewhere with a bit of tinkering (e.g. changing GitLab-specific variables).

## Comparison against other CNB implementations

### Pack

Pack is the defacto tool for CloudNative BuildPacks, however it is designed to be used by a developer.
It has a strict dependency on `docker`, which cannot be securely used in a container.

### [KPack](https://github.com/pivotal/kpack)

KPack is a Kubernetes-native tool for executing CNBs.
KPack is significantly more complex than this tool, and requires effort and maintenance work put into looking after Tekton and KPack infrastructure.

## Usage

As part of an AutoDevOps pipeline:
```yaml
---
# example of using CNB in a full Auto pipeline
variables:
  AUTO_CNB_RUN_IMAGE: gcr.io/paketo-buildpacks/run:tiny-cnb
  BUILD_KANIKO_DISABLED: "true"

include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/auto/Auto-GoMod.gitlab-ci.yml'
---
# example of including CNB in an existing pipeline
include:
  - remote: 'https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/build/AutoBuild.gitlab-ci.yml'
---
# example of extending the CNB job
auto build:
  stage: build
  dependencies: []
  extends: .auto-build
  before_script:
    - ./my-wacky-script.sh

include:
  remote: "https://gitlab.com/av1o/gitlab-ci-templates/-/raw/master/build/CNB.gitlab-ci.yml"
```

Manually:

```shell
docker run -it --entrypoint bash registry.gitlab.com/av1o/auto-build-packs/paketo-base:master
export CI_APPLICATION_IMAGE="docker.example.org/myrepo/myimage:mytag" /run.sh
```

## Configuration

> Cosign integration is experimental and disabled by default.

* `CNB_SIGN_ENABLED` - enables [Cosign](https://github.com/sigstore/cosign) integration
* `COSIGN_PRIVATE_KEY` - path to Cosign private key (note: it must be the `File` type)
* `COSIGN_PASSWORD` - password for Cosign private key

This project doesn't provide any buildpacks of its own, rather it enables the use of others inside a container.
The default builder is `paketo-base` and supports all configuration documented [here](https://paketo.io/docs/buildpacks/configuration/).